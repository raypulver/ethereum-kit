'use strict';

const sendRawTransaction = require('./send-raw-transaction');
const bufferToHex = require('./utils/buffer-to-hex');

const sendTxFromObject = async (rpc, tx) => await sendRawTransaction(rpc, bufferToHex(tx.serialize()));

module.exports = sendTxFromObject;
