'use strict';

module.exports = Object.create({
  getNextThenIncrement(provider) {
    const id = this[provider] || 0;
    this[provider] = id + 1;
    return id;
  }
});
