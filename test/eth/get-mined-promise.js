'use strict';

const setupBlockSubscription = require('../../setup-block-subscription');
const makeTransaction = require('../../make-transaction');
const getAccounts = require('../../get-accounts');
const getUnitHex = require('../../utils/get-unit-hex');
const generate = require('../../wallet/generate');

const {
  expect,
  spy
} = require('chai');

module.exports = ['should get a mined promise', async () => {
  const rpc = {
    provider: process.env.TESTRPC_URI || 'http://localhost:8545'
  };
  const blockPoller = setupBlockSubscription(rpc, 200)
  const [ builtInWallet ] = await getAccounts(rpc);
  const targetWallet = generate();
  const tx = await makeTransaction(rpc, {
    gasPrice: getUnitHex('1 gwei'),
    gasLimit: getUnitHex('21000'),
    to: targetWallet.getAddressString(),
    from: builtInWallet,
    value: getUnitHex('1 finney')
  }).sendTransaction();
  const promise = blockPoller.getMinedPromise(tx);
  const thenSpy = spy((result) => expect(result.hadError()).to.be.false);
  promise.then(thenSpy)
  await promise;
  expect(thenSpy).to.have.been.called;
}];
