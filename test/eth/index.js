const requireAll = require('../../internal/require-all');
const forOwn = require('../../internal/for-own');

module.exports = ['ethereum-sdk eth functions', (it) => () => forOwn(requireAll(__dirname, ['index.js', 'get-transaction-hash.js']), ([ description, suite ]) => it(description, suite))];
  
