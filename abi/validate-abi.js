'use strict';

module.exports = (abi) => {
  let type;
  if ((type = typeof abi) !== 'object') throw Error('expected abi definition to be an object, got ' + type);
  if (abi.name === undefined) throw Error('must supply name');
  if (!Array.isArray(abi.inputs)) throw Error('expected inputs to be an array');
  abi.inputs.forEach((v, i) => {
    if (typeof v !== 'object') throw Error('expected inputs[' + String(i) + ']'  + 'to be a string');
    if (typeof v.type !== 'string') throw Error('expected inputs[' + String(i) + '].type to be a string');
  });
  return abi;
};
