'use strict';

const callToRational = require('./call-to-number');

const getAllowance = (rpc, token, holder, spender) => callToRational(rpc, {
  to: token,
  data: encodeFunctionCall({
    name: 'allowance',
    inputs: [{
      name: 'holder',
      type: 'address'
    }, {
      name: 'spender',
      type: 'address'
    }]
  }, [ holder, spender ])
});

module.exports = getAllowance;
