'use strict';

const {
  isHex
} = require('web3-utils');

module.exports = isHex;
