'use strict';

const {
  padRight
} = require('web3-utils');

module.exports = padRight;
