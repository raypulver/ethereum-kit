'use strict';

const { 
  rightPad
} = require('web3-utils');

module.exports = rightPad;
