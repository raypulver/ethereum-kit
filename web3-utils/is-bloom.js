'use strict';

const {
  isBloom
} = require('web3-utils');

module.exports = isBloom;
