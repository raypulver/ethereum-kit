'use strict';

const {
  isTopic
} = require('web3-utils/src/utils');

module.exports = isTopic;
