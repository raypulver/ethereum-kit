'use strict';

const {
  toChecksumAddress
} = require('web3-utils');

module.exports = toChecksumAddress;
