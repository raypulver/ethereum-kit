'use strict';

const {
  hexToBytes
} = require('web3-utils');

module.exports = hexToBytes;
