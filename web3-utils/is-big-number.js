'use strict';

const {
  isBigNumber
} = require('web3-utils');

module.exports = isBigNumber;
