'use strict';

const {
  isBN
} = require('web3-utils');

module.exports = isBN;
