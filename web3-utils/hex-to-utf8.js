'use strict';

const {
  hexToUtf8
} = require('web3-utils');

module.exports = hexToUtf8;
