'use strict';

const {
  fromUtf8
} = require('web3-utils');

module.exports = fromUtf8;
