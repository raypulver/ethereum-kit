'use strict';

const {
  keccak256
} = require('web3-utils');

module.exports = keccak256;
