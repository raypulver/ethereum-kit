'use strict';

const {
  sha3
} = require('web3-utils');

module.exports = sha3;
