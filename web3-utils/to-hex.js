'use strict';

const {
  toHex
} = require('web3-utils');

module.exports = toHex;
