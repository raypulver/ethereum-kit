'use strict';

const {
  hexToNumberString
} = require('web3-utils');

module.exports = hexToNumberString;
