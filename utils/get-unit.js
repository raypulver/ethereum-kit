'use strict';

const Rational = require('tough-rational');
const unitMap = require('../unit-map')
const map = require('../internal/map');
const flow = require('../internal/flow');
const method = require('../internal/method');
const negate = require('../internal/negate');
const whiteSpaceRe = /\s+/;
const splitByWhiteSpace = method('split', whiteSpaceRe);
const toTrim = method('trim');
const mapToTrim = map(toTrim);
const castAndSplit = flow(String, splitByWhiteSpace);
const castSplitAndTrim = flow(castAndSplit, mapToTrim);
const isNumber = negate(isNaN);

module.exports = (qty, decimals = 18) => {
  const [ howMany, unit ] = castSplitAndTrim(qty);
  const factor = unitMap[unit];
  if (isNumber(howMany) && factor !== undefined) return Rational(howMany).multiply(factor).toString(decimals);
  return qty;
};
