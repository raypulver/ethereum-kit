'use strict';

const bufferToHex = require('./buffer-to-hex');
const toBuffer = require('./to-buffer');
const setLengthLeft = require('./set-length-left');
const {
  Buffer: {
    concat
  }
} = require('safe-buffer');

const toRpcSig = (v, r, s) => {
  if (v !== 27 && v !== 28) throw new Error('Invalid recovery id');
  return bufferToHex(concat([setLengthLeft(toBuffer(r), 32), setLengthLeft(toBuffer(s), 32), toBuffer(v - 27)]));
};

module.exports = toRpcSig;
