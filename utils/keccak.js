'use strict';

const toBuffer = require('./to-buffer');
const createKeccakHash = require('keccak');
const keccak = (a, bits = 256) => createKeccakHash('keccak' + bits).update(toBuffer(a)).digest();

module.exports = keccak;
