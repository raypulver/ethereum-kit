'use strict';

const {
  Buffer: {
    allocUnsafe
  }
} = require('safe-buffer');

const zeros = (bytes) => allocUnsafe(bytes).fill(0);

module.exports = zeros;
