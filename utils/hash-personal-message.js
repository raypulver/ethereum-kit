'use strict';

const toBuffer = require('./to-buffer');
const bufferToHex = require('./buffer-to-hex');
const keccak = require('./keccak');
const {
  Buffer: {
    concat
  }
} = require('safe-buffer');

const hashPersonalMessage = (message) => bufferToHex(keccak(concat([ toBuffer('\x19Ethereum Signed Message:\n' + message.length.toString()), toBuffer(message) ])));

module.exports = hashPersonalMessage;
