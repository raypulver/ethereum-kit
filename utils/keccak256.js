'use strict';

const keccak = require('./keccak');

const keccak256 = (v) => keccak(v);

module.exports = keccak256;
