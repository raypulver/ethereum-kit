'use strict';

const constant = require('../internal/constant');

module.exports = constant('0x' + Array(41).join(0));
