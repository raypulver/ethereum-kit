'use strict';

const toBuffer = require('./to-buffer');
const publicToAddress = require('./public-to-address');
const privateToPublic = require('./private-to-public');
const flow = require('../internal/flow');

module.exports = flow(toBuffer, privateToPublic, publicToAddress);
