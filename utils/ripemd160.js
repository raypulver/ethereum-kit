'use strict';

const toBuffer = require('./to-buffer');
const setLength = require('./set-length');
const createHash = require('create-hash');

const ripemd160 = (a, padded) => padded ? setLength(createHash('rmd160').update(toBuffer(a)).digest(), 32) : createHash('rmd160').update(toBuffer(a)).digest();

module.exports = ripemd160;
