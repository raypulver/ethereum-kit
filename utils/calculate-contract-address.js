'use strict';

const rlp = require('rlp');
const bufferToHex = require('./buffer-to-hex');
const addHexPrefix = require('./add-hex-prefix');
const soliditySha3 = require('../web3-utils/solidity-sha3');

module.exports = (address, nonce = 0) => addHexPrefix(soliditySha3({
  t: 'bytes',
  v: bufferToHex(rlp.encode([address, Number(nonce)]))
}).substr(26));
