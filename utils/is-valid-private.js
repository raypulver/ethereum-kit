'use strict';

const secp256k1 = require('secp256k1');
const toBuffer = require('./to-buffer');

const isValidPrivate = (privateKey) => secp256k1.privateKeyVerify(toBuffer(privateKey));

module.exports = isValidPrivate;
