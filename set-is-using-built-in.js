'use strict';

const { isUsingBuiltIn } = require('./is-using-built-in');
const set = require('./internal/set');

module.exports = (rpc, flag) => set(rpc, isUsingBuiltIn, flag);
