'use strict';

const bindObject = require('./internal/bind-object');
const { EthereumRPC } = require('./rpc');
const instance = EthereumRPC('http://localhost:8545');

module.exports = bindObject(instance);
