'use strict';

const zipObject = require('./internal/zip-object');
const getBlockNumber = require('./get-block-number');
const getTransactionReceipt = require('./get-transaction-receipt');
const getTransaction = require('./get-transaction');
const wrapRpc = require('./wrap-rpc');
const noop = require('./internal/noop');
const timeout = require('./internal/timeout');

const zipTransaction = zipObject([ 'transaction', 'receipt' ]);

function TransactionResult(data) {
  if (!(this instanceof TransactionResult)) return new TransactionResult(data);
  Object.assign(this, data);
}

Object.assign(TransactionResult.prototype, {
  hadError() {
    return this.receipt.status !== undefined && !this.receipt.status;
  }
});

const POLL_INTERVAL = 1000;

const BlockSubscriberSuperProto = {
  getRPC() {
    return this._rpc;
  },
  setRPC(rpc) {
    this._rpc = rpc;
    return this;
  },
  setPolling(flag) {
    this._polling = flag;
    return this;
  },
  getPolling() {
    return this._polling;
  },
  setPollInterval(interval) {
    this._pollInterval = interval;
    return this;
  },
  getPollInterval() {
    return this._pollInterval;
  },
  setErrHandler(errHandler) { 
    this._errHandler = errHandler;
    return this;
  },
  getErrHandler() {
    return this._errHandler;
  },
  getBlockNumber() {
    return this._number;
  },
  setBlockNumber(n) {
    this._number = n;
    return this;
  },
  getSubscribers() {
    return this._subscribers || [];
  }
};

BlockSubscriber.prototype = Object.assign(Object.create(BlockSubscriberSuperProto), {
  async getMinedPromise(txHash) {
    const [ blockNumber, receipt ] = await Promise.all([
      getBlockNumber(this.getRPC()),
      getTransactionReceipt(this.getRPC(), txHash)
    ]);
    if (receipt && Number(receipt.blockNumber) <= blockNumber) return TransactionResult(zipTransaction(await Promise.all([
      getTransaction(this.getRPC(), txHash),
      Promise.resolve(receipt)
    ])));
    return new Promise((resolve, reject) => {
      const unsubscribe = this.subscribe(async (blockNumber) => {
        const receipt = await getTransactionReceipt(this.getRPC(), txHash);
        if (receipt && Number(receipt.blockNumber) <= blockNumber) {
          unsubscribe();
          return resolve(TransactionResult(zipTransaction(await Promise.all([
            getTransaction(this.getRPC(), txHash),
            Promise.resolve(receipt)
          ]))));
        }
      });
    });
  },
  startPollingForBlocks(onErr = noop) {
    this.setErrHandler(onErr);
    this.setPolling(true);
    this.pollOne();
  },
  async pollOne() {
    try {
      const blockNumber = await getBlockNumber(this.getRPC());
      if (blockNumber > (this.getBlockNumber() || 0)) {
        this.setBlockNumber(blockNumber);
        this.fireSubscribers();
      }
    } catch (e) {
      this.getErrHandler()(e);
    } finally {
      await timeout(this.getPollInterval());
      if (this.getPolling()) return this.pollOne();
    }
  },
  stopPollingForBlocks() {
    this.setPolling(false);
  },
  fireSubscribers() {
    this.getSubscribers().forEach((v) => v.cb(this.getBlockNumber()));
    return this;
  },
  subscribe(cb) {
    this._subscribers = this._subscribers || [];
    const o = { cb };
    this.getSubscribers().push(o);
    return () => {
      const subscribers = this.getSubscribers();
      const i = subscribers.findIndex((v) => v === o);
      if (!~i) return false;
      subscribers.splice(0, 1);
      return true;
    };
  },
  subscribeToBlockChange(fn) {
    return this.subscribe(fn);
  }
});

function BlockSubscriber(rpc, pollInterval = POLL_INTERVAL) {
  if (!(this instanceof BlockSubscriber)) return new BlockSubscriber(rpc, pollInterval);
  this.setPollInterval(pollInterval);
  this.setRPC(rpc);
}

module.exports = BlockSubscriber;
