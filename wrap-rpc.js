'use strict';

const setProvider = require('./set-provider');

const makeDefaultRpc = (s) => {
  const rpc = {};
  setProvider(rpc, s);
  return rpc;
};

const wrapRpc = (rpc) => typeof rpc === 'string' ? makeDefaultRpc(rpc) : rpc;

module.exports = wrapRpc;
