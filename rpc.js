'use strict';

const mapToShifted = require('./internal/map-to-shifted');
const restoreConstructor = require('./internal/restore-constructor');

function EthereumRPC(provider) {
  if (!(this instanceof EthereumRPC)) return new EthereumRPC(provider);
  this.init(provider);
}

Object.assign(EthereumRPC, {
  idMap: require('./id-map'),
  unitMap: require('./unit-map')
});

const EthereumRPCSuperProto = mapToShifted({
  getProvider: require('./get-provider'),
  setProvider: require('./set-provider'),
  setInterceptor: require('./set-interceptor'),
  getInterceptor: require('./get-interceptor'),
  init: require('./init')
});

EthereumRPC.prototype = Object.create(EthereumRPCSuperProto);
restoreConstructor(EthereumRPC);

Object.assign(EthereumRPC.prototype, mapToShifted({
  rpcCall: require('./rpc-call'),
  setupBlockSubscription: require('./setup-block-subscription'),
  callForBlockToAddress: require('./call-for-block-to-address'),
  callForBlockToNumber: require('./call-for-block-to-number'),
  callForBlockToRational: require('./call-for-block-to-rational'),
  callForBlock: require('./call-for-block'),
  callToAddress: require('./call-to-address'),
  callToNumber: require('./call-to-number'),
  callToPrecision: require('./call-to-precision'),
  callToRational: require('./call-to-rational'),
  call: require('./call'),
  testForAllowance: require('./test-for-allowance'),
  getAccounts: require('./get-accounts'),
  getAllowance: require('./get-allowance'),
  getBlockNumberHex: require('./get-block-number-hex'),
  getBlockNumber: require('./get-block-number'),
  getCode: require('./get-code'),
  getEtherBalanceForBlockHex: require('./get-ether-balance-for-block-hex'),
  getEtherBalanceForBlock: require('./get-ether-balance-for-block'),
  getEtherBalanceHex: require('./get-ether-balance-hex'),
  getEtherBalance: require('./get-ether-balance'),
  getTokenBalance: require('./get-token-balance'),
  getTransactionCountForBlockHex: require('./get-transaction-count-for-block-hex'),
  getTranasctionCountForBlock: require('./get-transaction-count-for-block'),
  getTransactionCountHex: require('./get-transaction-count-hex'),
  getTransactionCount: require('./get-transaction-count'),
  getTransactionReceipt: require('./get-transaction-receipt'),
  sendRawTransaction: require('./send-raw-transaction'),
  sendSignedTransaction: require('./send-signed-transaction'),
  sendTransaction: require('./send-transaction'),
  sendTxFromObject: require('./send-tx-from-object'),
  ownerOfNFT: require('./owner-of-nf-token'),
  getBlock: require('./get-block'),
  isUsingBuiltIn: require('./is-using-built-in'),
  setIsUsingBuiltIn: require('./set-is-using-built-in'),
  makeTransaction: require('./make-transaction'),
  fetchNonceMakeTransaction: require('./fetch-nonce-make-transaction'),
  estimateGas: require('./estimate-gas')
}));

Object.assign(module.exports, {
  EthereumRPC
});
