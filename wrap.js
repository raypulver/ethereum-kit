'use strict';

const map = require('./internal/map');
const Rational = require('tough-rational');
const addHexPrefix = require('./utils/add-hex-prefix'); 
const wrapRational = (fn) => async (...args) => Rational(await fn(...args));
const mapWrapRational = map(wrapRational);
const wrapAddress = (fn) => async (...args) => addHexPrefix((await fn(...args)).substr(26));

const mapWrapAddress = map(wrapAddress);

const wrapNumber = (fn) => async (...args) => Number(await fn(...args));
const mapWrapNumber = map(wrapNumber);
const wrapPrecision = (fn) => async (...args) => Rational(await fn(...args)).toString();
const mapWrapPrecision = map(wrapPrecision);

Object.assign(module.exports, {
  wrapRational,
  mapWrapRational,
  wrapAddress,
  mapWrapAddress,
  wrapNumber,
  mapWrapNumber,
  wrapPrecision,
  mapWrapPrecision
});
