'use strict';

const property = require('./internal/property');

module.exports = property('interceptor');
