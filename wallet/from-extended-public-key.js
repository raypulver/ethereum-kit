'use strict';

const {
  fromExtendedPublicKey
} = require('ethereumjs-wallet');
const toBuffer = require('../utils/to-buffer');

const flow = require('../internal/flow');

module.exports = flow(toBuffer, fromExtendedPublicKey);
