'use strict';

const {
  fromPrivateKey
} = require('ethereumjs-wallet');
const toBuffer = require('../utils/to-buffer');

const flow = require('../internal/flow');

module.exports = flow(toBuffer, fromPrivateKey);
