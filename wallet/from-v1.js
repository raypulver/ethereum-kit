'use strict';

const {
  fromV1
} = require('ethereumjs-wallet');

module.exports = fromV1;
