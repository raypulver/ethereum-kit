'use strict';

const generateFunctionTest = require('./generate-function-test');

const testForAllowance = generateFunctionTest('allowance(address,address)');

module.exports = testForAllowance;
