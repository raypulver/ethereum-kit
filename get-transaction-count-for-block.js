'use strict';

const wrap = require('./wrap');
const getTransactionCountForBlockHex = require('./get-transaction-count-for-block-hex');
module.exports = wrap.wrapRational(getTransactionCountForBlockHex);
