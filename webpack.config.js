'use strict'

const _ = require('lodash');
const fp = require('lodash/fp');
const path = require('path');
const bindToPath = _.partial(_.bindKey, path);
const mapBindToPath = fp.map(bindToPath);
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const [
  join,
  parse
] = mapBindToPath([
  'join',
  'parse'
]);

Object.assign(module.exports, {
  mode: 'production',
  entry: join(__dirname, 'index.js'),
  output: {
    path: join(__dirname, 'dist'),
    libraryTarget: 'umd',
    umdNamedDefine: true,
    filename: process.env.MINIFY ? 'ethereum-sdk.min.js' : 'ethereum-sdk.js'
  },
  devtool: 'source-map',
  module: {
    rules: [{
      test: /(?:\.js$)/,
      use: ['babel-loader'],
      exclude: /node_modules/
    }, {
      test: /\.json$/,
      use: ['compact-json-loader'],
      exclude: /node_modules/
    }]
  },
  resolve: {
    extensions: ['.js', '.json']
  },
  plugins: process.env.MINIFY ? [ new UglifyJsPlugin() ] : []
});
