'use strict';

const enumeratePrototypeChain = require('./enumerate-prototype-chain');
const mapOwnNonEnumerable = require('./map-own-non-enumerable');

const bindObject = (o) => enumeratePrototypeChain(o).reduce((r, v) => Object.assign(Object.create(r), mapOwnNonEnumerable((fn, key) => (...args) => fn.apply(o, args))(v)), null);

module.exports = bindObject;
