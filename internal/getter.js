'use strict';

module.exports = (prop) => function () { return this[prop]; };
