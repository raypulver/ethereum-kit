'use strict';

const rearg = (ary) => (fn) => function (...args) { 
  fn.apply(this, ary.map((v, i) => args[i]));
};

module.exports = rearg;
