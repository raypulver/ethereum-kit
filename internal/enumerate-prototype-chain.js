'use strict';

const enumeratePrototypeChain = (o) => {
  const retval = [];
  while ((o = Object.getPrototypeOf(o))) {
    retval.push(o);
  }
  return retval.reverse();
};

module.exports = enumeratePrototypeChain;
