'use strict';

const assert = (flag) => {
  if (!flag) throw Error('Assertion failed');
};

module.exports = assert;
